{-# LANGUAGE GADTs #-}
module Algomas.Search.Constraint where

import Data.Text

{-|
A data type for comparing and building run-time complexity terms.

For the purposes of algomas, the most important operations it must
support are those exposed by Eq and Ord.

TODO: pick up some more algebra knowledge

-}
data Term
 = Constant                               -- O(1)
 | Variable                               -- O(n)
 | Logarithm Base Term                    -- O(log_base Term)
 | Exponential Numerator Denominator Term -- O(Term^k, O(Term^(1/k))
 | Factorial Term                         -- O(Term!)
 | Plus Term Term                         -- O(Term+Term)
 | Minus Term Term                        -- O(Term-Term)
 | Mult Term Term                         -- O(Term*Term)
 | Div Term Term                          -- O(Term/Term)

-- | canonicalize Term repr to facilitate comparison
simplify :: Term -> Term
simplify Constant = Constant
simplify Variable = Variable
simplify (Logarithm b t) = Logarithm b (simplify t)
simplify (Exponential n d t) = Exponential n d (simplify t)
simplify (Factorial t) = Factorial (simplify t)
simplify (Plus t1 t2) = _
simplify (Minus t1 t2) = _
simplify (Mult t1 t2) = _
simplify (Div t1 t2) = _

{-|

Big-O w/ simplified syntax and the like for complexity
searching. Big-O notation is all about describing the boundary on how
an operation's run-time will grow compared to the input size. Most
operations are specified in terms of how they behave at all
times. Other operations are given bounds in terms of how they behave
*most* of the time. So that amounts to 6 types of bounds:

* Exact: =
* Upper: <
* Lower: >
* AvgExact: ~=
* AvgUpper: ~<
* AvgLower: ~>

<= and >= can be specified in terms of an Or constraint:

(Or (UpperBound Variable) (ExactBound Variable))  -- <=

TODO: The interplay of non-Avg and Avg bounds needs some thought.

-}
data Bound
  = UpperBound Term    -- "<(n)"
  | LowerBound Term    -- ">(n)"
  | TightBound Term    -- "=(n)"

-- | How we want to match text
data SearchString
  = Exact Text  -- "cat"
  | Regex Text  -- ".*cat.*"
  | Prefix Text -- "cat~"
  | Suffix Text -- "~cat"

-- GADTs because it might be handy to place type restrictions on
-- composition of certain Constraints
-- AST: And (Structure "HashMap") (Complexity (UpperBound Constant))
-- WIP: Concrete: HashMap: - <(1)
data Constraint a where
  And        :: Constraint a -> Constraint a -> Constraint a
  Or         :: Constraint a -> Constraint a -> Constraint a
  Not        :: Constraint a -> Constraint a  -- "!O(n!)", "!insert"
  Structure  :: SearchString -> Constraint a  -- "HashMap:..."
  Operation  :: SearchString -> Constraint a  -- "...:insert"
  Complexity :: Bound -> Constraint a         -- "<(n)", ...

newtype Base = Base Int
newtype Numerator = Numerator Int
newtype Denominator = Denominator Int

{-|

Given a set of constraints, returns whether they could possibly be
satisfied. This is an involved problem, given the presence of
constraint combinators [And, Or, Not]. Some things, we'd obviously
like to reject:

>>> satisfiable (And (Exact "cat") (Exact "dog"))
False

>>> satisfiable (And (TightBound Variable) (TightBound Constant))
False

>>> satisfiable (And (TightBound Variable) (Not $ TightBound Variable))
False

>>> satisfiable (Or (Exact "cat") (Exact "dog"))
True

This is a reduced satisfiability problem. There are three types of
constraints we can express:

* Structure: what structure is sought out by name?
* Operation: what operation is sought out by name?
* Complexity: what complexity bounds are being sought?

There are three ways to join them:

* And: c1 & c2
* Or:  c1 | c2
* Not: !c

The combination of constraints is also conveniently enumerable:

* HashMap:insert - <(1) -- most specific - all three given
* :insert - <(1)        -- operation + complexity
* HashMap: - <(1)       -- structure + complexity
* <(1)                  -- complexity
* :insert               -- operation
* HashMap:              -- structure
* HashMap:insert        -- structure + operation
* <nil>                 -- no constraint specify; prevent this case

I'm not sure if this buys us anything yet. Just a thought.

REF: "A Rendezvous of Logic, Complexity, and Algebra: <http://arxiv.org/abs/cs/0611018>

-}
satisfiable :: Constraint a -> Bool
satisfiable _ = undefined
