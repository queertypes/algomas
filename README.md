# Algomas

Algomas is an algorithm and data structure search engine written in
Haskell. It will allow one to search for data structures using:

* Expected run-time complexity
* Structure name: "hash.*"
* Operation name: "insert", "delete"

# Searches

A search returns quite a bit of information. By default, you'll see
something like:

```
HashMap: [insert: O(1), delete: O(1), ...]
RB Tree: [insert: O(lg n), delete: O(lg n), ...]
List:    [append: O(1), insert: O(n), delete: O(n), ...]
```

Data structures are given priority in this arrangement. It'd be nice
to offer further customization on the output of a search. For example:

* Sort by operation name
* Prioritize operation, sort by run-time

```
insert: [HashMap: O(1), RB Tree: O(lg n), List: O(n), ...]
```

Here are some examples:

* `insert` searches for structures that contain an operation named exactly `insert`
* `hash.*` searches for structures that have a name that begins with `hash`
* `<(n)` searches for structures that have at least one operation with a complexity of `O(n)`
* `insert - <(n)` searches for structures containing an insert operation bound by `O(n)`

# Plans

There will be three components:

* Algomas Haskell library, which is the core to eveything else
* Algomas CLI
* Algomas web interface

The Algomas Haskell library is further divided into the following
components:

* Parser: translates concrete user syntax into internal types
* Constraint Engine: determines whether user constraints can be satisfied
* Search Engine: given satisfiable constraints, search knowledge base
* Knowledge Base: components for operating with data structure metadata
    * Knowledge Model: representation of data structure metadata
    * Knowledge Parser: going from metadata language to Algomas types
* Show: output data structure representation in forms more convenient to users

For `Show`, in particular, the first pass will output all data
structure types in a Haskelly format. It'd be wonderful to allow the
user to specify that structure in a language that is more familiar to
them:

* C
* Clojure
* Racket
* Scala
* Rust
* Java
* Javascript
* Ruby
* Python
* Erlang

As an example, when introspecting a particular data structure more deeply with the search engine:

```haskell
List.insert :: Ord a => a -> [a] -> [a]
```

Then again in C:

```c
void* insert(void* /* comparable */ x, void* list);
```

# Code of Conduct

This project abides by the excellent
[Contributor's Convenant](http://contributor-covenant.org/). Please
read the included `CODE_OF_CONDUCT.md`.
